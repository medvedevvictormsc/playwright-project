Заметки по проекту для дальнейшей интеграции в рабочий процесс

Обновление версии PW:
npm install -D @playwright/test@latest

// @ts-check 
Добавляется в начало каждого тестового файла при использовании JavaScript в VS Code, чтобы получить автоматическую проверку типов.

Команды PW:
await - проставляется перед командами если надо чтобы PW дождался их выполнения

Простые взаимодействия

1. await page.goto('https://playwright.dev/'); - перейти на указанную страницу
2. page.locator('scc-selector') - перейти к какому то элементу по селектору
3. locator().click() - Кликнуть по элементу
4. locator().hover() - Навестись на элемент
5. locator().fill('text') - ввести текст в элемент
6. locator().press() - кликнуть по элементу

Проверки - https://playwright.dev/docs/test-assertions прочие проверки

1. await expect(locator('scc-selector')).toBeVisible() - проветка что элемент отображается
2. await expect(locator('scc-selector')).toBeHidden() - проветка что элемент не отображается
3. await expect(locator('scc-selector')).toContainText('текст') - проветка что элемент содержит текст
4. await expect(locator('scc-selector')).toHaveValue('текст') - проветка что элемент содержит значение
5. await expect(locator('scc-selector')).toHaveAttribute('текст') - проветка что элемент содержит атрибут
6. await expect(locator('scc-selector')).toHaveClass('текст') - проветка что элемент содержит класс