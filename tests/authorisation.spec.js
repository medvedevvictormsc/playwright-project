// @ts-check

import { test, expect } from "@playwright/test";
import { allure } from "allure-playwright";
import { Severity } from "allure-js-commons";



test('Пользователь может ввести значение в поле "Логин"', async ({page}, testInfo) => {

    await allure.id('@132');
    await allure.description('Этот тест проверяет работу поля "Логин"');
    await allure.owner("Виктор медвевев");
    await allure.tags("smoke", "regression");
    await allure.severity(Severity.CRITICAL);

    await allure.step("Шаг 1", async () => {
        await page.goto('https://cc-new.test-sbermegamarket.ru/sign-in');
        await allure.step("Шаг 2", async () => {
            await expect(page.locator('input[name="login"]')).toBeVisible();
        });
    });
    
});