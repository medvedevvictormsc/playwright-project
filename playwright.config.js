const { defineConfig, devices } = require('@playwright/test');
import { testPlanFilter } from "allure-playwright/dist/testplan";

module.exports = defineConfig({
  projects: [
    {
      name: "Chromium",
      use: { ...devices["Desktop Chrome"] },
    }
  ],
  grep: testPlanFilter(),
  reporter: [
    ["line"],
    [
      "allure-playwright",
      {
        outputFolder: "allure-results",
        suiteTitle: true,
        detail: false,
        environmentInfo: {},
      },
    ],
  ],
});
